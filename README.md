# MoodleNet Federated Server 




## About the project

This is the MoodleNet back-end, written in Elixir (running on the Erlang VM, and using the Phoenix web framework). The client API uses GraphQL. The federation API uses [ActivityPub](http://activitypub.rocks/). The MoodleNet front-end is built with React (in a [seperate repo](https://gitlab.com/moodlenet/clients/react)).

This codebase was forked from [CommonsPub](http://commonspub.org/) (project to create a generic federated server, based on the `ActivityPub` and `ActivityStreams` web standards) which was originally forked from [Pleroma](https://git.pleroma.social/pleroma/pleroma). All three projects are [AGPL](https://www.gnu.org/licenses/agpl-3.0.en.html) licensed.

## Doc index

Do you wish to deploy MoodleNet? Read our [Deployment Guide](https://gitlab.com/moodlenet/servers/federated/blob/develop/DEPLOY.md).

Do you wish to develop MoodleNet? Read our [Developer Guide](https://gitlab.com/moodlenet/servers/federated/blob/develop/HACKING.md).

## Copyright and License

MoodleNet: Connecting and empowering educators worldwide

Copyright © 2018-2019 Moodle Pty Ltd <https://moodle.com/moodlenet/>

Contains code from Pleroma <https://pleroma.social/> and CommonsPub <https://commonspub.org/>

Licensed under the GNU Affero GPL version 3.0 (GNU AGPLv3).

